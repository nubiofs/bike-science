import json
import geohash2 as gh
import math

from .tiers import separate_into_tiers
from .arrow import draw_arrow


def extract_flows_and_tiers(trips, precision=7):

    def encode_od_points(row):
        lats = json.loads(row.lats)
        if len(lats) < 2: return None, None, None, None, None, None
        longs = json.loads(row.longs)
        return gh.encode(lats[0], longs[0], precision=precision), \
               gh.encode(lats[-1], longs[-1], precision=precision), \
               lats[0], longs[0], lats[-1], longs[-1]

    trips[['start_hash', 'end_hash', 'start_lat', 'start_lon', 'end_lat', 'end_lon']] = \
            trips.apply(encode_od_points, axis=1, result_type='expand')
    od_counts = trips.groupby(['start_hash', 'end_hash'], as_index=False) \
                      .agg({'tripid': 'count',
                            'start_lat': 'mean', 'start_lon': 'mean',
                            'end_lat': 'mean', 'end_lon': 'mean'})
    od_counts.rename(columns={'tripid': 'trip counts'}, inplace=True)
    od_counts.sort_values('trip counts', ascending=False, inplace=True)
    tiers4, _ = separate_into_tiers(od_counts, None, None, max_tiers=4)
    return od_counts, tiers4


POPUP_NUM_TRIPS = 0
POPUP_FLOW_ID = 1

def flow_map(fmap, od_df, minimum=-1, maximum=-1, show=4, radius=1.0, text=POPUP_NUM_TRIPS):

    # eliminate round-trips to the same station, which are not considering in this analysis
    filtered = od_df[od_df.start_hash != od_df.end_hash]

    if maximum == -1:
        maximum = filtered['trip counts'].max()
    if minimum == -1:
        minimum = maximum / show
        
    total_trips = filtered['trip counts'].sum()

    filtered = filtered[((filtered['trip counts'] >= minimum) & (filtered['trip counts'] <= maximum))]

    shown_trips = 0
    
    for idx, row in filtered.iterrows():
        num_trips = row['trip counts']
        
        shown_trips += num_trips
        weight = math.ceil( (num_trips-minimum)/maximum * 10)
        if weight == 0: weight = 1
        
        if text == POPUP_NUM_TRIPS:
            text_plot = str(num_trips) + ' bike trips'
        else:
            text_plot = 'Start: ' + row['start_hash'] + ' / End: ' + row['end_hash']

        try:
            draw_arrow(fmap, row.start_lat, row.start_lon, row.end_lat, row.end_lon, 
                   text=text_plot,
                   weight=weight,
                   radius_fac=radius)
        except Exception as e:
            print(row)
            raise e

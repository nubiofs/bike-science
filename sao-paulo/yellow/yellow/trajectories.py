import json
from shapely.geometry import LineString, Point
import geopandas as gpd
import folium


SAO_PAULO = [-23.5489, -46.6388]


def process_trajectories(trips, columns=[]):
    def generate_line_geometry(row):
        lats = json.loads(row.lats)
        longs = json.loads(row.longs)
        if len(lats) >= 2:
            return LineString(zip(longs, lats))
        if len(lats) == 1:
            return Point(longs[0], lats[0])
        return None
    data = {'tripid': trips.tripid}
    for c in columns: data[c] = trips[c]
    geometry = trips.apply(generate_line_geometry, axis=1)
    return gpd.GeoDataFrame(data, geometry=geometry, crs={'init': 'epsg:4326'})

def create_map(center=SAO_PAULO, zoom=12):
    fmap = folium.Map(center, 
                      zoom_start=zoom, 
                      control_scale=True,
                      tiles='https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png', 
                      attr='&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> &copy; <a href="https://carto.com/attributions">CARTO</a>')
    return fmap


def plot_trip(fmap, trip, plot_path=True, plot_points=True):
    lats = json.loads(trip.lats)
    longs = json.loads(trip.longs)
    if plot_path:
        points = zip(lats, longs)
        ant = next(points)
        for p in points:
            folium.PolyLine(locations=[ant, p], color='blue', weight=3).add_to(fmap)
            ant = p
    if plot_points:
        points = zip(lats, longs)
        ant = next(points)
        for p in points:
            folium.CircleMarker(p, radius=1, color='red').add_to(fmap)
            ant = p
from matplotlib import pyplot as plt
import matplotlib.ticker as tkr
import matplotlib.dates as mdate


# global settings for the charts
"""
plt.rcParams['figure.titlesize'] = 26
plt.rcParams['font.size'] = 22
plt.rcParams['axes.labelsize'] = 20
plt.rcParams['axes.labelweight'] = 'bold'
plt.rcParams['xtick.labelsize'] = 20
plt.rcParams['ytick.labelsize'] = 20
plt.rcParams['axes.grid.axis'] = 'y'
plt.rcParams['axes.grid'] = True
plt.rcParams['legend.fontsize'] = 18
"""


def numbers_in_thousands(num,pos):
    num = num/1000
    num = int(num)
    return '{}'.format(num)

THOUSANDS_FORMATTER = tkr.FuncFormatter(numbers_in_thousands)


def scale_in_thousands(ax, x=False, y=False):
    if x: ax.xaxis.set_major_formatter(THOUSANDS_FORMATTER)
    if y: ax.yaxis.set_major_formatter(THOUSANDS_FORMATTER)
        

def log_scale(x=False, y=False):
    if x: plt.xscale('log')
    if y: plt.yscale('log')


def dist_chart(series, title, unit):
    ax = plt.axes()
    ax.set_axisbelow(True)
    plt.grid(linestyle='--')
    ax.xaxis.grid(False)
    plt.hist(bins=500, x=series)
    plt.title(title)
    plt.xlabel(unit)
    plt.ylabel('# trips')
    return ax
    
    
def time_scatter(series, title, unit, time_series):
    plt.title(title)
    plt.ylabel(unit)
    plt.scatter(time_series, series, alpha=0.2)
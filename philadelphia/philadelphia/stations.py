import pandas as pd
from shapely.geometry import Point
import geopandas as gpd
import folium

def from_trips(trips):
    """
    Extracts a bike stations list from the trips dataframe. Returns a GeoDataframe containing the station location points.
    """
    start_stations = trips[['start station id', 'start station latitude', 'start station longitude']]
    start_stations.columns = ['id', 'lat', 'lon']

    end_stations = trips[['end station id', 'end station latitude', 'end station longitude']]
    end_stations.columns = ['id', 'lat', 'lon']

    stations = pd.concat([start_stations, end_stations])
    stations.dropna(inplace=True)

    stations.drop_duplicates(subset='id', inplace=True)
    stations.set_index('id', inplace=True)
    stations.sort_index(inplace=True)

    stations['geometry'] = stations.apply(lambda row: Point(row['lon'], row['lat']), axis=1)
    return gpd.GeoDataFrame(stations, crs={'init': 'epsg:4326'})


def draw_stations(folium_map, stations):
    """
    Plots the station points on a Folium map.
    """
    for index, row in stations.iterrows():
        folium.CircleMarker(location=[row.lat, row.lon], radius=1,
                            popup=str(index) + ': ' + str(row.lat) + ', ' + str(row.lon), 
                            color='black').add_to(folium_map)
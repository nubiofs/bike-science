import pandas as pd
import sys
import glob

sys.path.append("..")

from bikescience import load_trips as tr

def load_trips_file(file):
    trips = pd.read_csv(file, parse_dates=['start_time', 'end_time'])
    trips.rename(columns={
                    'duration': 'tripduration', 
                    'start_time': 'starttime', 
                    'end_time': 'stoptime', 
                    'start_station': 'start station id',
                    'end_station': 'end station id',
                    'start_lat': 'start station latitude',
                    'end_lat': 'end station latitude',
                    'start_lon': 'start station longitude',
                    'end_lon': 'end station longitude',
                 }, 
                 inplace=True)
    tr.create_time_features(trips)
    return trips

def load_trips_files(glob_pattern):
    files = glob.glob(glob_pattern)
    trip_dfs = []
    for f in files:
        trip_dfs.append(load_trips_file(f))
    return pd.concat(trip_dfs, sort=True)